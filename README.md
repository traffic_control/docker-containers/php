# PHP docker images

All dev images have xdebug and composer pre-installed.

## docker-compose usage example

```yaml
version: "3"
services:
  php:
    user: ${UID}
    image: gitlab.credy.eu:5001/docker/php:7.3-apache-dev
    volumes:
      - .:/var/www/html
    ports:
      - 80:80
    sysctls:
      - net.ipv4.ip_unprivileged_port_start=0 
```
